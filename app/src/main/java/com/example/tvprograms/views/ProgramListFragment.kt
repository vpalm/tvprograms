package com.example.tvprograms.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.tvprograms.R
import com.example.tvprograms.domain.ProgramListViewModel
import kotlinx.android.synthetic.main.program_list_fragment.*

class ProgramListFragment : Fragment() {

    private val model: ProgramListViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        activity?.title = "Most viewed programs"
        return inflater.inflate(R.layout.program_list_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(context)
        val viewAdapter = ProgramListAdapter()

        programList.adapter = viewAdapter
        programList.layoutManager = layoutManager

        model.liveData.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                viewAdapter.submitList(it.list)
                swipeToRefresh.isRefreshing = it.loading
            }
        })

        swipeToRefresh.setOnRefreshListener {
            model.getPrograms()
        }

    }


}