package com.example.tvprograms.views

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.tvprograms.R
import com.example.tvprograms.domain.models.Program
import kotlinx.android.synthetic.main.programs_list_item.view.*
import java.net.URLEncoder
import java.text.DateFormat
import java.time.OffsetDateTime
import java.util.*

class ProgramListAdapter : ListAdapter<Program, ProgramListAdapter.ProgramViewHolder>(Program.DIFF_UTIL) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProgramViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.programs_list_item, parent, false)

       return ProgramViewHolder(view)
    }

    override fun onBindViewHolder(holder: ProgramViewHolder, position: Int) {
       holder.bind(getItem(position))
    }

    class ProgramViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bind(program: Program) {
            itemView.program_title.text = program.title
            itemView.program_description.text = program.description
            itemView.program_broadcast_date_time.text = formatDateTime(program.broadcastDateTime)
            val urlEncodedImageUrl = URLEncoder.encode(program.imageUrl, "utf-8")
            Glide.with(itemView).load("https://imageproxy.b17g.services/convert?source=${urlEncodedImageUrl}").into(itemView.program_image)
        }

        private fun formatDateTime(dateTime: String) : String {
            val offSetDateTime = OffsetDateTime.parse(dateTime)
            val date = Date.from(offSetDateTime.toInstant())
            return DateFormat.getDateTimeInstance().format(date)
        }
    }
}