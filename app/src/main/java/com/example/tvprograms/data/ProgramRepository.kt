package com.example.tvprograms.data

import com.example.tvprograms.data.network.ProgramService
import com.example.tvprograms.domain.models.Program
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ProgramRepository {

    private val programService : ProgramService

    init {
        val retrofit = Retrofit.Builder()
            .baseUrl("https://sslapi.tv4play.se")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        programService = retrofit.create(ProgramService::class.java)
    }

    suspend fun getPrograms(): List<Program>{
        val mostViewedResponse = withContext(Dispatchers.IO) {
            programService.getMostViewedPrograms()
        }
        return mostViewedResponse?.results?: listOf()
    }
}