package com.example.tvprograms.data.models

import com.example.tvprograms.domain.models.Program

data class MostViewedResponse(val total_hits: Int, val results: List<Program>)