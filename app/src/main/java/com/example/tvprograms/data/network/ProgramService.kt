package com.example.tvprograms.data.network

import com.example.tvprograms.data.models.MostViewedResponse
import retrofit2.http.GET

interface ProgramService {

    @GET("/play/video_assets/most_viewed")
    suspend fun getMostViewedPrograms(): MostViewedResponse?
}