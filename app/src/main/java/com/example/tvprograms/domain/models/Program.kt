package com.example.tvprograms.domain.models

import androidx.recyclerview.widget.DiffUtil
import com.google.gson.annotations.SerializedName

data class Program (private val id : Int,
                    val title: String,
                    val description: String,
                    @SerializedName ("broadcast_date_time")
                    val broadcastDateTime: String,
                    @SerializedName("image")
                    val imageUrl: String) {
    companion object {
        val DIFF_UTIL = object : DiffUtil.ItemCallback<Program>() {
            override fun areItemsTheSame(oldItem: Program, newItem: Program): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Program, newItem: Program): Boolean {
                return oldItem == newItem
            }

        }
    }
}