package com.example.tvprograms.domain.models

data class ProgramListResult (val loading: Boolean = true, val list: List<Program> = emptyList())