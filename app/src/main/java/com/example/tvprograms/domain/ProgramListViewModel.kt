package com.example.tvprograms.domain

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.tvprograms.data.ProgramRepository
import com.example.tvprograms.domain.models.ProgramListResult
import kotlinx.coroutines.launch

class ProgramListViewModel : ViewModel() {

    private val programRepository = ProgramRepository()

    private val _liveData = MutableLiveData<ProgramListResult>()
    val liveData: LiveData<ProgramListResult> = _liveData

    init {
        getPrograms()
    }

    fun getPrograms() {
        _liveData.value = _liveData.value?.copy(loading = true)?: ProgramListResult()
        viewModelScope.launch {
            val list = programRepository.getPrograms()
            _liveData.value = ProgramListResult(false, list)
        }
    }
}